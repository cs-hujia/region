package com.hujia.rs.redis;

import com.hujia.context.VertxContext;

import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;

public class RedisProvider {
	
	private static RedisClient client  = null;
	private RedisProvider() {};
	
	public static RedisClient getRedisClient() {
		if (client == null) {
			RedisOptions options = new RedisOptions();
			options.setHost("localhost").setPort(6379).setAuth("123456");
			client = RedisClient.create(VertxContext.vertx(), options);
		}
		
		return client;
	}
	
}
