package com.hujia.router;

import com.hujia.context.VertxContext;
import com.hujia.handler.CityHandler;
import com.hujia.handler.ProvinceHandler;
import com.hujia.handler.RegionHandler;

import io.vertx.ext.web.Router;

public class RegionRouter {

	private Router router = null;

	public RegionRouter() {
		if (router == null) {
			router = Router.router(VertxContext.vertx());
		}
	}

	public Router getRouter() {
		return router;
	}

	// 加载所有的路由
	public void loadRouter() {

		RegionHandler regionHandler = new RegionHandler();
		this.router.get("/init/data").handler(regionHandler::init);

		ProvinceHandler provinceHander = new ProvinceHandler();
		this.router.get("/province/list").handler(provinceHander::list);
		this.router.get("/province/children").handler(provinceHander::children);

		CityHandler cityHandler = new CityHandler();
		this.router.get("/city/children").handler(cityHandler::children);
	}

}
