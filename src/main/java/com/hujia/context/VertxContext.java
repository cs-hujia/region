package com.hujia.context;

import io.vertx.core.Vertx;

public class VertxContext {
	
	private static Vertx vertx;
	private VertxContext() {};
	
	public static Vertx vertx() {
		if (vertx == null) {
			vertx = Vertx.vertx();
		}
		
		return vertx;
	}
	
}
