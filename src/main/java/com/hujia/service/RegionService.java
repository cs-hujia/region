package com.hujia.service;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

public class RegionService {
	
	private ProvinceService provinceService = new ProvinceService();
	private CityService cityService = new CityService();
	private AreaService areaService = new AreaService();
	
	
	public Future<Void> initData() {
		
		Future<Void> future = Future.future();
		
		List<Future> futureList = new ArrayList<Future>();
		futureList.add(provinceService.loadProvinceData());
		futureList.add(cityService.loadCityData());
		futureList.add(areaService.loadAreaData());
		
		CompositeFuture.all(futureList).setHandler(rs -> {
			if (rs.failed()) {
				future.fail(rs.cause().getMessage());
				return;
			}
			future.complete();
		});
		
		
		return future;
	}
	
}
