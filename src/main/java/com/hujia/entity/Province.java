package com.hujia.entity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.hujia.rs.redis.RedisProvider;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;

/**
 * 省
 * 
 * @author xiaomi
 *
 */
public class Province {

	private String code; // 行政编码
	private String name; // 行政名称

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void sacn(JsonObject jsonObject) throws Exception {
		if (jsonObject == null) {
			new Exception("转换省对象失败，json对象为空.");
		}

		this.code = jsonObject.getString("code");
		this.name = jsonObject.getString("name");
	}

	
}
