package com.hujia.entity;
/**
 * 城市
 * @author xiaomi
 *
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.hujia.rs.redis.RedisProvider;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;

public class City {
	private String code;  //行政编码
	private String name;  //行政名称 
	private String provinceCode; //省 编号

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	
	
	public void scan(JsonObject jo) {
		this.code = jo.getString("code");
		this.name = jo.getString("name");
		this.provinceCode = jo.getString("provinceCode");
	}
	
	


}
