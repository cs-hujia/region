package com.hujia.handler;

import com.hujia.service.RegionService;

import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

public class RegionHandler {
	
	private RegionService regionService = new RegionService();
	
	public void init(RoutingContext routingContext) {
		
		HttpServerResponse w = routingContext.response();
		
		Future<Void> future = regionService.initData();
		future.setHandler(rs -> {
			if (rs.succeeded()) {
				w.end("succeeed");
				return;
			}
			
			w.setStatusCode(500);
			w.end(rs.cause().getMessage());
		});
	}
	
	
}
