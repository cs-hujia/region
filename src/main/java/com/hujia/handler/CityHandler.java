package com.hujia.handler;

import java.util.List;

import com.hujia.entity.Area;
import com.hujia.service.CityService;

import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

public class CityHandler {
	
	private CityService cityService = new CityService();
	
	/**
	 * 获取某一个省份下的城市
	 */
	public void children(RoutingContext routingContext) {

		HttpServerResponse w = routingContext.response();

		String cityCode = routingContext.request().getParam("code");
		if (cityCode == null || cityCode.equals("")) {
			w.setStatusCode(400);
			w.setStatusMessage("城市编号不能为空.");
			w.end();
			return;
		}
		
		Future<List<Area>> future = cityService.children(cityCode);
		future.setHandler( rs -> {
			if (rs.succeeded()) {
				List<Area> areas = rs.result();
				w.putHeader("content-type", "application/json");
				w.end(Json.encode(areas));
				return;
			}
			
			w.setStatusCode(500);
			w.end(rs.cause().getMessage());
		});
	}
}
