package com.hujia;

import com.hujia.context.VertxContext;
import com.hujia.router.RegionRouter;
import io.vertx.core.http.HttpServer;

public class RegionApplication {

	public static void main(String[] args) {

		// 开http的服务
		HttpServer httpServer = VertxContext.vertx().createHttpServer();

		RegionRouter router = new RegionRouter();
		router.loadRouter();

		httpServer.requestHandler(router.getRouter()::accept);
		httpServer.listen(3002, res -> {
			if (res.succeeded()) {
				System.out.println("服务启动成功.");
				return;
			}
			System.out.println("服务启动失败.");

		});
	}
}
